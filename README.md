- This GIT repository contains 2 main folders:
	- assignment (application code base)
	- yii_framework (Yii framework 1.x)
	
	These 2 folders should be copied in web root e.g. /var/www/html
	
	It can be accessed as: http://localhost/assignment
	
- TODO: 
	- file: / assignment / protected / config / main.php
	 'lastfm_app_key' 	parameter should be supplied a valid Last.fm API app key

        - Directory: assignment/assets should be supplied 777 permission
        - Directory: assignment/protected/runtime should be supplied 777 permission
        
	
	
- Unit Tests:

	- phpunit.phar should be downloaded from https://phar.phpunit.de/phpunit.phar
	
	Execute tests (in assignment/protected/tests):
	
	$php phpunit.phar functional/