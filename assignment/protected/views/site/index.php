<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Home',
);
?>

<h1>Search</h1>

<div class="form">
    <form action="<?php echo $this->createUrl("site/search");?>" method="POST">

	<div class="row">
            <input type="text" value="<?php echo (isset($query) ? $query : ''); ?>" name="search_field" placeholder="Search Country">
	</div>
	

	<div class="row buttons">
            <input type="Submit" value="Search">
                        <?php echo (isset($contents) ? $contents : ''); ?>

	</div>
        
        <div class="row">
                   
	</div>

</form>
</div><!-- form -->
