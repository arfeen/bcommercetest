<?php

/**
 * WebApiManager is the component responsible for handling API 
 */
class WebApiManager extends CApplicationComponent {

    /**
     * This methos will called to fetch top artist list by country
     * @author Arfeen <arfeenster@gmail.com>
     * @param string $country
     * @param int $pageno
     * @param int $limit
     * @param int $pageblock
     * @return string
     * 
     */
    public function getTopArtistByCountry($country, $pageno = 1, $limit = 5, $pageblock = 1) {

        $pageblocksaved = $pageblock;
        $htmltable = "";

        $lastfmapi = "method=geo.gettopartists&country=" . $country;
        $contents = $this->getWebService($lastfmapi, $pageno, $limit);

        if (isset($contents->error)) {
            $htmltable = $contents->message;
            return $htmltable;
        }

        $htmltable = '<table width="30%" style="border-width: 5px !important; border-style: groove !important;"> 
            <tr>
                <th>Band</th>
                <th>Artwork</th>
            </tr>';

        if (!isset($contents->topartists->artist))
            return $htmltable;

        foreach ($contents->topartists->artist as $artistrecord) {

            $targeturl = Yii::app()->controller->createUrl("site/artisttracks", array("artistname" => $artistrecord->name));
            $url = "#text";
            $thumbnail = $artistrecord->image[0]->$url;
            $htmltable .= "<tr>";
            $htmltable .= "<td><a href='" . $targeturl . "'>" . $artistrecord->name . "</a></td>";
            $htmltable .= "<td><a href='" . $targeturl . "'><img src='" . $thumbnail . "'></a></td>";
            $htmltable .= "</tr>";
        }

        $url = Yii::app()->controller->createUrl("site/search");
        $attr = "@attr";
        $totalpages = $contents->topartists->$attr->totalPages;

        $nextpageblocks = "";
        $prevpageblocks = "";

        if ($totalpages > 20) {
            $nextpage = ($pageblock * 20) + 1;
            $pageblock++;
            $nextpageblocks = '<a href="' . $url . "?search_field=" . $country . "&page=" . $nextpage . "&pageblock=" . $pageblock . '"> >> </a>';
        }

        if ($pageblock > 1) {
            $prevpage = ($pageblocksaved * 20) - 20;
            $pageblock-=2;
            $prevpage = ($prevpage <= 0 ? $prevpage = 1 : $prevpage);
            $prevpageblocks = '<a href="' . $url . "?search_field=" . $country . "&page=" . $prevpage . "&pageblock=" . $pageblock . '"> << </a> | ';
        }

        $htmltable .= "<tr><td colspan='2'>";

        $htmltable .= $prevpageblocks;

        for ($nIndex = ($pageblocksaved * 20) - 19; $nIndex <= ($pageblocksaved * 20); $nIndex++) {
            $pagesurl = $url . "?search_field=" . $country . "&page=" . $nIndex . "&pageblock=" . $pageblocksaved;
            $htmltable .= "<a href='" . $pagesurl . "'>" . $nIndex . "</a> | ";
        }

        $htmltable .= $nextpageblocks;


        $htmltable .= " / " . $totalpages . " pages</td></tr>";

        return $htmltable;
    }

    /**
     * This methos will called to fetch top tracks by artist 
     * @author Arfeen <arfeenster@gmail.com>
     * @param string $artistname
     * @param int $pageno
     * @param int $limit
     * @param int $pageblock
     * @return string
     * 
     */
    public function getTopTracksByArtist($artistname, $pageno = 1, $limit = 10, $pageblock = 1) {

        $pageblocksaved = $pageblock;

        $htmltable = "";

        $lastfmapi = "method=artist.gettoptracks&artist=" . $artistname;
        $contents = $this->getWebService($lastfmapi, $pageno, $limit);

        if (isset($contents->error)) {
            $htmltable = $contents->message;
            return $htmltable;
        }

        $htmltable = '<table width="30%" style="border-width: 5px !important; border-style: groove !important;"> 
            <tr>
            <th colspan="2">Top 10 Records of ' . $artistname . '
            </tr>
            <tr>
                <th>Track</th>
                <th>Artwork</th>
            </tr>';


        foreach ($contents->toptracks->track as $trackrecord) {

            $url = "#text";
            $thumbnail = $trackrecord->image[0]->$url;
            $htmltable .= "<tr>";
            $htmltable .= "<td>" . $trackrecord->name . "</td>";
            $htmltable .= "<td><img src='" . $thumbnail . "'></td>";
            $htmltable .= "</tr>";
        }

        return $htmltable;
    }
    
    /**
     * Core function to get the API by providing rest API endpoint
     * @param type $restapimethod
     * @param type $pageno
     * @param type $limit
     * @return type
     */

    public function getWebService($restapimethod, $pageno = 1, $limit = 5) {

        $authapikey = Yii::app()->params['lastfm_app_key'];
        $restapibaseurl = "http://ws.audioscrobbler.com/2.0/?";

        $restapi = $restapibaseurl . $restapimethod . "&api_key=" . $authapikey . "&format=json&page=" . $pageno . "&limit=" . $limit;
        Yii::log($restapi, "info", "WEB_SERVICES");

        $response = @file_get_contents($restapi);

        if (!$response)
            return;

        return json_decode($response);
    }

}
