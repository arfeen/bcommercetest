<?php

class UnitTest extends CTestCase {

    public function testWebServiceAuth() {

        $authmethod = "method=user.getinfo&user=rj";
        $jsoncontents = Yii::app()->WebApiManager->getWebService($authmethod);

        $this->assertEquals($jsoncontents->user->name, "RJ");
    }

    public function testTopArtistByCountry() {

        $authmethod = "method=geo.gettopartists&country=australia";
        $jsoncontents = Yii::app()->WebApiManager->getWebService($authmethod);

        $this->assertTrue(isset($jsoncontents->topartists));
    }
    
    public function testTopTracks() {

        $authmethod = "method=artist.gettoptracks&&artist=Adele";
        $jsoncontents = Yii::app()->WebApiManager->getWebService($authmethod);

        $this->assertTrue(isset($jsoncontents->toptracks));
    }

}
